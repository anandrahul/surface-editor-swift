//
//  HomeView.swift
//  Surface Editor
//
//  Created by Anand Singh on 17/10/2020.
//

import SwiftUI

struct HomeView: View {
    // MARK: - Properties

    @State var size: CGFloat = 1.0
    @State var progress: CGFloat = 0.0
    @ObservedObject var viewModel: HomeViewModel

    let layout = [GridItem(.adaptive(minimum: 200))]

    private let imageViewWidth: CGFloat = 50;
    private let imageViewHeight: CGFloat = 50;
    private let padding: CGFloat = 30;

    // MARK: - Init

    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        VStack {
            NavigationView {
                VStack {
                    Text("Size")
                    Slider(value: $size, in: 1...2) {
                    }.padding(.horizontal, padding)
                    
                    Text("Progress")
                    Slider(value: $progress, in: 0...1) {
                    }.padding(.horizontal, padding)
                }
                
                ScrollView(.vertical){
                    LazyVGrid(columns: layout) {
                        ForEach(viewModel.gridItemModels, id: \.self) { item in
                            CustomImageView(imageObject: item,
                                            width: imageViewWidth * size,
                                            height: imageViewHeight * size,
                                            coefficient: size,
                                            baseUrl: viewModel.imageBaseUrl)
                                .padding()
                                .onTapGesture {
                                    print("Item tapped \(String(describing: item.image))")
                                }
                            /*
                             TODO : On item tap

                             DownloadStatusView(progress: progress)
                             .frame(width: 25 * (size / 2),
                             height: 25 * (size / 2))
                             */
                        }
                    }
                    .padding(.horizontal)
                }
            }
        }.onAppear() {
            viewModel.loadImages()
        }
    }
}
