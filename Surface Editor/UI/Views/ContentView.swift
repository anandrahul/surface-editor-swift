//
//  ContentView.swift
//  Surface Editor
//
//  Created by Anand Singh on 17/10/2020.
//

import SwiftUI

struct HomeView: View {
    @State var size: CGFloat = 1.0
    @State var progress: CGFloat = 0.0
    
    @State var data = Array(1...100).map {
        GridItemModel(title: "item \($0)",
                      isDownloading: false)
    }
    let layout = [GridItem(.adaptive(minimum: 200))]
    
    var body: some View {
        VStack {
            NavigationView {
                VStack {
                    Text("Size")
                    Slider(value: $size, in: 1...2) {
                    }.padding(.horizontal, 30)
                    
                    Text("Progress")
                    Slider(value: $progress, in: 0...1) {
                    }.padding(.horizontal, 30)
                }
                
                ScrollView(.vertical){
                    LazyVGrid(columns: layout) {
                        ForEach(data, id: \.self) { item in
                            VStack(alignment: .center) {
                                Image("pattern")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 100 * size,
                                           height: 200 * size)
                                
                                if !item.isDownloading {
                                    Button(action: {
                                        guard let index = data.firstIndex(of: item) else { return }
                                        self.data[index] = GridItemModel(title: item.title, isDownloading: true)
                                    }, label: {
                                        Image(systemName: "icloud.and.arrow.down")
                                    })
                                } else {
                                    DownloadStatusView(progress: progress)
                                        .frame(width: 25 * (size / 2),
                                               height: 25 * (size / 2))
                                }
                                
                                Text(item.title)
                                    .font(.system(size: 10 * size))
                                
                            }
                        }
                    }
                    .padding(.horizontal)
                }
            }
        }.onAppear() {
            ConcreteImageDownloder.init(apiKey: "79319da5-8cb3-43ac-f5b0-f38a727242a8", baseUrl: "https://www.livesurface.com/test/api/images.php")
                .getImages()
                .sink { (completion) in

                } receiveValue: { (images) in
                    //TODO - load all list
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
