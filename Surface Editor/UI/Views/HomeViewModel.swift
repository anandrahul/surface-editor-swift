//
//  HomeViewModel.swift
//  Surface Editor
//
//  Created by Anand Singh on 20/10/2020.
//

import Combine
import Foundation

class HomeViewModel: ObservableObject {
    // MARK: - Properties

    @Published var gridItemModels = [GridItemModel]()
    @Published var imagesData = [ImageObject]()
    let imageBaseUrl: String;
    
    private var task: AnyCancellable?
    private let downloader: ImageDownloader

    // MARK: - Init

    init(imageDownloader: ImageDownloader, imageBaseUrl: String) {
        downloader = imageDownloader
        self.imageBaseUrl = imageBaseUrl
    }

    // MARK: - Function

    func loadImages() {
        task = downloader.getImages()
            .receive(on: RunLoop.main)
            .sink { (completion) in
                switch completion {
                case .finished:
                    print("successfully downloaded image data")
                case .failure(let error):
                    print("failed to download image data with error : \(error)")
                }
            } receiveValue: { [weak self] (images) in
                guard let self = self else { return }
                self.imagesData = images
                self.gridItemModels = images.map { GridItemModel(title: $0.image,
                                                                 name: $0.name,
                                                                 number: $0.number,
                                                                 image: $0.image,
                                                                 category: $0.category) }
            }
    }

    // MARK: - Deinit

    deinit {
        task?.cancel()
    }
}
