//
//  CustomImageView.swift
//  Surface Editor
//
//  Created by Anand Singh on 20/10/2020.
//

import Foundation

import SwiftUI

struct CustomImageView: View {
    private let imageObject: GridItemModel
    @ObservedObject private var imageLoader: Loader
    private let placeholder = UIImage(named: "placeholder")!
    private let width: CGFloat
    private let height: CGFloat
    private let coefficient: CGFloat
    
    var image: UIImage? {
        imageLoader.data.flatMap(UIImage.init)
    }
    
    init(imageObject: GridItemModel, width: CGFloat, height: CGFloat, coefficient: CGFloat, baseUrl: String) {
        self.imageObject = imageObject
        self.width = width
        self.height = height
        self.coefficient = coefficient
        self.imageLoader = Loader(URL(string: baseUrl + (imageObject.image ?? ""))!)
    }
    
    var body: some View {
        VStack {
            Image(uiImage: image ?? placeholder)
                .resizable()
                .scaledToFill()
                .frame(width: width, height: height)
            Text(imageObject.title)
                .font(.system(size: 10 * coefficient))
        }
    }
}
