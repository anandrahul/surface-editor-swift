//
//  DownloadStatusView.swift
//  Surface Editor
//
//  Created by Anand Singh on 17/10/2020.
//

import SwiftUI

struct DownloadStatusView: View {
    // MARK: - Properties

    @State var progress: CGFloat = 0.0
    let color = Color.gray
    let backgroundColor = Color.gray.opacity(0.3)

    let style = StrokeStyle(lineWidth: 2, lineCap: .round)

    var body: some View {
        ZStack {
            Circle()
                .stroke(AngularGradient(gradient: .init(colors: [backgroundColor]),
                                        center: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/), style: style)
            Circle()
                .trim(from: 0.0, to: self.progress)
                .stroke(AngularGradient(gradient: .init(colors: [color]),
                                        center: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/), style: style)
                .animation(.easeIn, value: true)
        }
    }

    // MARK: - Functions

    func set(progress: CGFloat) {
        self.progress = progress
        if self.progress >= 1 {
            self.progress = 0
        }
    }
}

struct DownloadStatusView_Preview: PreviewProvider {
    static var previews: some View {
        let status = DownloadStatusView(progress: 0.7)
        status.set(progress: 1.0)
        return status
    }
}

