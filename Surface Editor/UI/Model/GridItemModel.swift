//
//  GridItemModel.swift
//  Surface Editor
//
//  Created by Anand Singh on 17/10/2020.
//

import Foundation

struct GridItemModel: Hashable {
    let title: String
    var name: String?
    var number: String?
    var image: String?
    var category: String?
}
