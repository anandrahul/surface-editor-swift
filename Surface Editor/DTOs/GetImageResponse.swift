//
//  GetImageResponse.swift
//  Surface Editor
//
//  Created by Anand Singh on 17/10/2020.
//

import Foundation

struct GetImageResponse: Decodable {
    // MARK: - Properties
    
    let images: [String: ImageObject]
}
