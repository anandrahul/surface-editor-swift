//
//  Tags.swift
//  Surface Editor
//
//  Created by Anand Singh on 17/10/2020.
//

import Foundation

struct Tags: Codable {
    // MARK: - Properties
    
    let sizedescription: String
    let sizescale: String
    let sizewidth: String
    let sizewidtharc: String
    let sizeheight: String
    let sizeheightarc: String
    let sizedepth: String
    let sizedeptharc: String
    let sizeunits: String
}
