//
//  Image.swift
//  Surface Editor
//
//  Created by Anand Singh on 17/10/2020.
//

import Foundation

struct ImageObject: Codable {
    // MARK: - Properties
    
    var index: Int
    var name: String
    var number: String
    var image: String
    var category: String
    var version: String
    var tags: Tags
}
