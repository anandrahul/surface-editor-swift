//
//  Surface_EditorApp.swift
//  Surface Editor
//
//  Created by Anand Singh on 17/10/2020.
//

import SwiftUI

@main
struct Surface_EditorApp: App {
    var body: some Scene {
        WindowGroup {
            let imageDownloader = ConcreteImageDownloder(apiKey: "79319da5-8cb3-43ac-f5b0-f38a727242a8",
                                                         baseUrl: "https://www.livesurface.com/test/api/images.php")
            HomeView(viewModel: HomeViewModel(imageDownloader: imageDownloader,
                                              imageBaseUrl: "https://www.livesurface.com/test/images/"))
        }
    }
}
