//
//  ConcreteImageDownloder.swift
//  Surface Editor
//
//  Created by Anand Singh on 17/10/2020.
//

import Combine
import Foundation

final class ConcreteImageDownloder: ImageDownloader {
    // MARK: - Properties

    private let apiKey: String
    private let baseUrl: String?

    // MARK: - Init

    init(apiKey: String, baseUrl: String) {
        self.apiKey = apiKey
        self.baseUrl = baseUrl
    }

    // MARK: - Functions

    func getImages() -> AnyPublisher<[ImageObject], ImageDownloaderError> {
        guard let baseUrl = baseUrl, var urlComponents = URLComponents(string: baseUrl) else {
            fatalError("Forgot to set correct baseUrl. Please check!")
        }

        urlComponents.queryItems = [
            URLQueryItem(name: "key", value: apiKey)
        ]

        guard let url = urlComponents.url else { fatalError("Could not created request!") }
        let request = URLRequest(url: url)

        return Future<[ImageObject], ImageDownloaderError> { promise in
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    fatalError("Error: \(error.localizedDescription)")
                }
                guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                    fatalError("Error: invalid HTTP response code")
                }
                guard let data = data else {
                    fatalError("Error: missing response data")
                }

                do {
                    let decoder = JSONDecoder()
                    let imageDownloadResponse = try decoder.decode(GetImageResponse.self, from: data)
                    var images = [ImageObject]()
                    imageDownloadResponse.images.forEach { (key, imageObject) in
                        images.append(imageObject)
                    }
                    promise(.success(images))
                }
                catch {
                    print("Error: \(error.localizedDescription)")
                }
            }
            task.resume()
        }.eraseToAnyPublisher()
    }
}
