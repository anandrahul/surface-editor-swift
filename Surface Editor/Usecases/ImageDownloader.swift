//
//  ImageDownloader.swift
//  Surface Editor
//
//  Created by Anand Singh on 17/10/2020.
//

import Combine

enum ImageDownloaderError: Error {
    case failed
}

protocol ImageDownloader {
    func getImages() -> AnyPublisher<[ImageObject], ImageDownloaderError>
}


