//
//  ImageLoader.swift
//  Surface Editor
//
//  Created by Anand Singh on 20/10/2020.
//

import SwiftUI
import Combine

final class Loader: ObservableObject {
    @Published var data: Data? = nil
    var task: URLSessionDataTask!

    init(_ url: URL) {
        task = URLSession.shared.dataTask(with: url, completionHandler: { data, _, _ in
            DispatchQueue.main.async {
                self.data = data
            }
        })
        task.resume()
    }
    deinit {
        task.cancel()
    }
}
